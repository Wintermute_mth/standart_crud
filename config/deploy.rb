lock '3.3.5'

set :repo_url, 'git@bitbucket.org:Wintermute_mth/standart_crud.git'

set :branch, 'master'

set :rails_env, 'production'

set :username, 'root'

set :application, 'standart_crud'

set :deploy_to, '/var/www/standart_crud'

set :keep_releases, 4

